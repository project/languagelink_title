
Language Link Title
This is a very simple module that adds a 'title'-attribute to all links in the 
language selector menu.

The title will consist of the native title of the language. The effect will be, 
that hovering over a language link will display the native language title. 
This is useful if you only want to display languageicons (e.g. flags) and no 
text.

